# Vim orgmode agenda
![image](screenshot.png)  

My implementation of orgmode agenda for Vim.  
It's just a module for printing agenda and actually is the part of what I use
for my planner in Vim. I will later publish other parts, such as Vim's config
and syntax highlighting.

## Features
- Print a list of todos from "org" files, in three modes: "week" (default) for
agenda view, "todo" and "wait" for list of todos with appropriate state.
- Print project name (as title of parent level) for todo.
- At the end of each todo line may be printed a mark with line number. I use it
to jump to the file and todo line.

## Usage
```
from viorg_agenda import view


if __name__ == "__main__":
    files = [/home/blahblah/org/personal.org", "/home/blahblah/org/work.org"]
    view(files, mode="week", line_num=True)
```
