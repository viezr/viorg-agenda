'''
Agenda printout module.
'''
from copy import deepcopy
from datetime import date, time, datetime, timedelta

from .states import DEADLINE, TODO, WAIT, WEEK_STATUS_LIST
from .todo import Todo



class PrintWorker():
    '''
    Todo print worker class.
    '''
    def __init__(self, todos_list: list, mode: str, line_num: bool = False):
        self._todos_list = todos_list
        self._mode = mode
        self._line_num = line_num
        self._week_dict = {"past": [], "future": []}

    def run(self) -> None:
        '''
        Print todos in chosen mode.
        '''
        if self._mode in (TODO, WAIT):
            self._print_status_todos()
            return
        self._print_week_agenda()

    def _print_status_todos(self) -> None:
        '''
        Print todos with status.
        '''
        for todo in self._todos_list:
            if todo.status == self._mode:
                self._print_todo(todo)

    def _print_week_agenda(self) -> None:
        '''
        Print week agenda.
        '''
        self._prepare_todos()
        today = date.today()
        weekday = date.weekday(today)
        monday = datetime.combine(today - timedelta(days=weekday), time(0,0,0,0))
        week_days = [monday + timedelta(days=x) for x in range(7)]
        self._set_week_dict(week_days, monday)
        self._print_week_out("past")
        self._print_week(week_days)
        self._print_week_out("future")

    def _prepare_todos(self) -> None:
        '''
        Create todo copies for multiple schedule dates in one todo.
        '''
        todo_copies = []
        for todo in self._todos_list:
            if len(todo.dates) <= 1:
                continue
            schedules = todo.dates
            for sched in schedules[1:]:
                todo_copy = deepcopy(todo)
                todo_copy.dates = [sched,]
                todo_copies.append(todo_copy)
            todo.dates = [schedules[0],]
        self._todos_list += todo_copies

    def _set_week_dict(self, week_days: list, monday: datetime) -> None:
        '''
        Create dictionary with past, future, and current week todos.
        '''
        next_monday = week_days[0] + timedelta(days=7)
        future_day_limit = next_monday + timedelta(days=7)
        for todo in self._todos_list:
            if not todo.dates or todo.status not in WEEK_STATUS_LIST:
                continue
            todo_sched = todo.dates[0]
            if todo_sched.stype == DEADLINE:
                todo.title = f"{todo.title} [D]"
            if todo_sched.date < monday:
                self._week_dict["past"].append(todo)
            elif next_monday <= todo_sched.date <= future_day_limit:
                self._week_dict["future"].append(todo)
            else:
                todo_date = date(todo_sched.date.year, todo_sched.date.month,
                    todo_sched.date.day)
                if todo_date in self._week_dict:
                    self._week_dict[todo_date].append(todo)
                else:
                    self._week_dict[todo_date] = [todo,]
        for key in ("past", "future"):
            self._week_dict[key].sort(key=lambda x: x.dates[0].date)

    def _print_week_out(self, out_type: str) -> None:
        '''
        Print outdated todos.
        '''
        title = "Missed or active tasks" if out_type == "past" else "Next week"
        print(title)
        if not self._week_dict[out_type]:
            print("  -")
            return
        for todo in self._week_dict[out_type]:
            self._print_todo(todo)

    def _print_week(self, week_days: list) -> None:
        '''
        Print current week todos.
        '''
        today = date.today()
        for idx, weekday in enumerate(week_days):
            title = ''.join(["W", weekday.strftime("%W")]) if idx == 0 else ""
            weekday_date = date(weekday.year, weekday.month, weekday.day)
            mark = "LTOD" if weekday_date == today and self._line_num else ""
            tags = ""
            weekday_str = "{:<12}{:<18}{:<50}{:<12}{:<4}".format(
                weekday.strftime("%A"), weekday.strftime("%d %B %Y"),
                title, tags, mark)
            print(weekday_str)
            if weekday_date in self._week_dict:
                for todo in self._week_dict[weekday_date]:
                    self._print_todo(todo)

    def _print_todo(self, todo: Todo) -> None:
        '''
        Print todo for agenda.
        '''
        project_len = 10
        tdate = ""
        if todo.dates:
            sched = todo.dates[0]
            tdate = sched.datetime_str()
        tags = f":{todo.tags}" if todo.tags else ""
        status = f"{todo.status} " if todo.status else ""
        urgent = f"[{todo.urgent}] " if todo.urgent else ""
        project = (f"[{todo.reference.project[:project_len - 1]}.] "
            if len(todo.reference.project) > project_len
            else f"[{todo.reference.project}] ")
        line = f"L{todo.reference.line}" if self._line_num else ""
        string = "  {:<10}{:<18}{:<13}{}{}{:<50}{:<12}{:<4}".format(
            todo.reference.file, tdate, project, status, urgent, todo.title, tags,
            line)
        print(string)
