'''
Todo model.
'''


class Todo():
    '''
    Todo model class.
    '''
    def __init__(self, status: str, urgent: str, title: str, tags: str):
        self.status = status
        self.urgent = urgent
        self.title = title
        self.tags = tags
        self.dates = []
        self.reference = None

    def __str__(self) -> str:
        return str(self.__dict__)

class TodoRef():
    '''
    Todo references class.
    '''
    def __init__(self, project: str, file: str, line: int):
        self.project = project
        self.file = file
        self.line = line
