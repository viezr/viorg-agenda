'''
Main parsing module for orgmode files.
'''
import os

from .todo import Todo, TodoRef
from .parsing_tools import get_dates, parse_headline


class ParseFileState():
    '''
    Parsing file state class.
    '''
    def __init__(self):
        self.headline = ""
        self.parent_title = ""
        self.content_line = 0
        self.current_level = 0
        self.current_todo = None

class ParseFile():
    '''
    Parsing file class for creating todos.
    '''
    def __init__(self, file: str, data: str):
        self.data = data
        self.file_name = os.path.basename(file).split(".org")[0]
        self.content_lines_limit = 3
        self.state = ParseFileState()
        self._todos = []

    def get_todos(self) -> list:
        '''
        Parse file for todos and scheduled entries.
        '''
        for line_num, line in enumerate(self.data):
            if line.startswith("*"):
                self._process_headline(line)
            elif self.state.content_line > 0:
                self._process_content(line, line_num)
        return self._todos

    def _process_headline(self, line: str) -> None:
        '''
        Headline process for string.
        '''
        new_level = len(line.split("* ", 1)[0]) + 1
        if new_level > self.state.current_level:
            _status, _ulevel, self.state.parent_title, _tags = parse_headline(
                self.state.headline)
        self.state.headline = line
        self.state.current_level = new_level
        self.state.content_line = self.content_lines_limit
        self.state.current_todo = None

    def _process_content(self, line: str, line_num: int) -> None:
        '''
        Content process for string.
        '''
        if not self.state.current_todo:
            status, ulevel, title, tags = parse_headline(self.state.headline)
            self.state.current_todo = Todo(status, ulevel, title, tags)
            self.state.current_todo.reference = TodoRef(self.state.parent_title,
                self.file_name, line_num)
            self._todos.append(self.state.current_todo)
        dates = get_dates(line)
        if dates:
            self.state.current_todo.dates = dates
        self.state.content_line -= self.content_lines_limit if dates else 1
