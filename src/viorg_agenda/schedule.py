'''
Schedule time model
'''
from datetime import datetime, timedelta


class Schedule():
    '''
    Schedule time model
    '''
    def __init__(self, date: datetime, stype: str, delta: timedelta):
        self.date = date
        self.stype = stype
        self.rep_delta = delta or timedelta(days=0)

    def date_str(self) -> str:
        '''
        Get string of date.
        '''
        return self.date.strftime("%Y-%m-%d")

    def datetime_str(self) -> str:
        '''
        Get string of date and time.
        '''
        if any((self.date.hour, self.date.minute)):
            return self.date.strftime("%Y-%m-%d %H:%M")
        return self.date_str()
