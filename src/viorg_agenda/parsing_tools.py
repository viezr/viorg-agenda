'''
Parsing tools for orgmode files.
'''
from datetime import datetime, timedelta

from .schedule import Schedule
from .states import SCHEDULED, DEADLINE
from .states import STATUS_LIST
from .states import URG_A, URG_B, URG_C



def get_dates(line: str) -> list:
    '''
    Parse dates.
    '''
    stypes = (SCHEDULED, DEADLINE)
    line = line.lower().strip()
    sched_list = [_get_date(line, stype) for stype in stypes if stype in line]
    return sched_list

def _get_date(line: str, stype: str) -> Schedule:
    '''
    Parse line for provided type of data schedule.
    '''
    time = None
    line = line.split(stype, 1)[1]
    date, *repeat = line.split(">", 1)[0].split("<", 1)[1].rsplit(" ", 1)
    if repeat:
        repeat = repeat[0]
    if repeat and ":" in repeat:
        time, repeat = repeat, None
    if " " in date and not ":" in date:
        date = date[:10]
    date = datetime.fromisoformat(f"{date} {time}" if time else date)
    repeat = _get_repeat(repeat)
    sched = Schedule(date, stype, repeat)
    return sched

def _get_repeat(line: str = None) -> timedelta:
    '''
    Get repeat timedelta from part of timestamp.
    '''
    if not line:
        return timedelta(hours=0)
    rep_dict = {"m": "days", "d": "days", "h": "hours"}
    try:
        key = rep_dict[line[-1]]
        delta_num = int(line.split("+")[1][:-1])
    except (KeyError, IndexError, TypeError):
        key = rep_dict.get("h")
        delta_num = 0
    delta = timedelta(**{key: delta_num})
    return delta

def parse_headline(line: str) -> tuple:
    '''
    Pase line for status, title and tags.
    '''
    status = ulevel = title = tags = ""
    line = line.strip("* ")
    line, status = _get_status(line)
    line, ulevel = _get_urgent_level(line)
    title, tags = _get_title_tags(line)
    return (status, ulevel, title, tags)

def _get_status(line: str) -> tuple:
    '''
    Get todo status from line.
    '''
    status = [x for x in STATUS_LIST if x in line]
    if not status:
        return (line, "")
    line = line.split(status[0], 1)[1].strip()
    return (line, status[0])

def _get_urgent_level(line: str) -> tuple:
    '''
    Get todo urgent level from line.
    '''
    if not "]" in line:
        return (line, "")
    ulevel, line = line.split("]", 1)
    ulevel = ulevel.split("[", 1)[1]
    ulevel = ulevel if ulevel in (URG_A, URG_B, URG_C) else ""
    line = line.strip()
    return line, ulevel

def _get_title_tags(line: str) -> tuple:
    '''
    Get todo title and tags from line.
    '''
    if not ":" in line:
        return (line, "")
    title, tags = line.split(":", 1)
    tags.strip()
    title = title.strip()
    return (title, tags)
