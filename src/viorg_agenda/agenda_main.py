'''
Agenda view for orgmode files
'''
from .parse_file import ParseFile
from .print_worker import PrintWorker
from .states import TODO, WAIT, WEEK


def view(files: list, mode: str = "week", line_num: bool = False) -> None:
    '''
    Agenda entry point.
    '''
    mode = mode.upper()
    args = (TODO, WEEK, WAIT)
    if mode not in args:
        print(f"Error. Agenda type '{mode}' not found in {args}")
        return
    todos_list = []
    for file in files:
        with open(file, encoding="utf-8") as filed:
            data = filed.read().split("\n")
        file_processor = ParseFile(file, data)
        todos_list += file_processor.get_todos()
    print_worker = PrintWorker(todos_list, mode, line_num)
    print_worker.run()
